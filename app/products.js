const express = require('express');
const router = express.Router();

const createRouter = (db) => {
  // Product index
  router.get('/', (req, res) => {
    res.send(db.getData());
  });

  // Product create
  router.post('/', (req, res) => {
    const product = req.body;

    db.addItem(product).then(result => {
      res.send(result);
    });
  });

  // Product get by ID
  router.get('/:id', (req, res) => {
    res.send(db.getDataById(req.params.id));
  });

  return router;
};

module.exports = createRouter;